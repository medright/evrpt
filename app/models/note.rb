class Note < ActiveRecord::Base
	has_many :comments, as: :commentable
	belongs_to :user
	belongs_to :provider, class_name: 'User', foreign_key: :provider_id
	#attachment :note_image
end
