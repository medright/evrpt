class CreateNotes < ActiveRecord::Migration[5.0]
  def change
    create_table :notes do |t|
      t.string :service
      t.text :subjective
      t.text :objective
      t.text :assessment
      t.text :plan
      t.string :note_image

      t.timestamps
    end
  end
end
