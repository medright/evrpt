Rails.application.routes.draw do
  resources :orders
  resources :notes
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks", registrations: "users/registrations" }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'static_pages#home'

  resources :users
  resources :static_pages do
  	collection do
		  get :new_consumer
		  post :create_consumer
		end
	end
end
