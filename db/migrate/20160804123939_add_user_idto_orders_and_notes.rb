class AddUserIdtoOrdersAndNotes < ActiveRecord::Migration[5.0]
  def change
  	add_reference :orders, :user, index: true
  	add_reference :notes, :user, index: true
  end
end
