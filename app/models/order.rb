class Order < ActiveRecord::Base
  belongs_to :user
  belongs_to :provider, class_name: 'User', foreign_key: :provider_id
end
