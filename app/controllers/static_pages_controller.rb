class StaticPagesController < ApplicationController
  def home
    if user_signed_in?
      @users = User.all.consumer
    end
  end

  def new_consumer
    @user = User.new
  end

  def create_consumer
    current_user.consumers.create(user_params)
    redirect_to root_path
  end

  private

  def user_params
    params[:user][:role] = 2
  	params.require(:user).permit(:email, :password, :approved, :role, :provider_id, :uid)
  end
end
