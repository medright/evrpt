class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
 
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :lockable, :timeoutable, :omniauthable, :omniauth_providers => [:google_oauth2]

  enum role: %w(provider admin consumer)

  has_many :consumers, class_name: "User",
                       foreign_key: "provider_id"

  belongs_to :provider, class_name: "User"

  has_many :orders, dependent: :destroy
  has_many :notes, dependent: :destroy

  def active_for_authentication?
    super && approved?
  end

  def inactive_message
   if !approved?
     :not_approved
   else
     super # Use whatever other message
   end
  end

  def self.send_reset_password_instructions(attributes={})
    recoverable = find_or_initialize_with_errors(reset_password_keys, attributes, :not_found)
    if !recoverable.approved?
      recoverable.errors[:base] << I18n.t("devise.failure.not_approved")
    elsif recoverable.persisted?
      recoverable.send_reset_password_instructions
    end
    recoverable
  end

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.auth_provider = auth.provider
      user.uid = auth.uid
      user.email = auth.info.email
      user.password = Devise.friendly_token[0, 20]
      user.provider = user
      #user.approved = true
      #user.save!
      user.save!(validate: false)
    end
  end

  def self.new_with_session(params, session)
    if session["devise.user_attributes"]
      new(session["devise.user_attributes"], without_protection: true) do |user|
        user.attributes = params
        user.valid?
      end
    else
      super
    end
  end
end
