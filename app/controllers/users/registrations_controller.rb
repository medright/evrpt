class Users::RegistrationsController < Devise::RegistrationsController

	def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    errors = User.create(user_params).errors
    respond_to do |format|
      if errors.one? && errors[:provider].present?
      	@user.save(validate: false)
        @user.update_attributes(provider: @user)
        format.html { redirect_to root_path, notice: 'User was successfully created.' }
      else
      	format.html { redirect_to 'new' }
      	format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def user_params
  	params.require(:user).permit(:email, :password, :password_confirmation, :approved, :provider_id)
  end
end