class AddProviderIdToConsumerProviders < ActiveRecord::Migration[5.0]
  def change
    add_reference :consumer_providers, :provider, foreign_key: true
  end
end
