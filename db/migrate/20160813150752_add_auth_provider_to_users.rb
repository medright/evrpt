class AddAuthProviderToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :auth_provider, :string
  end
end
