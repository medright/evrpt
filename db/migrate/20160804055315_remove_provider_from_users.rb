class RemoveProviderFromUsers < ActiveRecord::Migration[5.0]
  def change
  	remove_column :users, :provider
  	remove_column :users, :consumer_id
  	remove_column :users, :provider_id
  end
end
