class AddProviderIdToOrdersAndNotes < ActiveRecord::Migration[5.0]
  def change
  	add_reference :orders, :provider, index: true
  	add_reference :notes, :provider, index: true
  end
end
