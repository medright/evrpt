class AddConsumerIdAndProviderIdToUsers < ActiveRecord::Migration[5.0]
  def change
  	add_reference :users, :consumer, index: true
  	add_reference :users, :provider, index: true
  end
end
