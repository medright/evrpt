class CreateConsumerProviders < ActiveRecord::Migration[5.0]
  def change
    create_table :consumer_providers do |t|
      t.references :user, foreign_key: true
      t.references :consumer, foreign_key: true

      t.timestamps
    end
  end
end
